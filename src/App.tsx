import React from "react";
import "./App.css";
import BulkNotificationSender from "./components/bulk-notification-sender";
import { SnackbarProvider } from "notistack";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import BulkNotificationSenderV2 from "./components/bulk-notification-sender-v2";

const router = createBrowserRouter([
  {
    path: "/",
    element: <BulkNotificationSender />,
  },
  {
    path: "/v2",
    element: <BulkNotificationSenderV2 />,
  },
]);

function App() {
  return (
    <div className="App">
      <SnackbarProvider maxSnack={3}>
        <RouterProvider router={router} />
      </SnackbarProvider>
    </div>
  );
}

export default App;
