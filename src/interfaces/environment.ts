export interface IEnviroment {
  phones: string[];
  urls: string[];
  labels: string[];
  open: boolean;
  getFromAirtable: boolean;
  airtableData: any[];
  selectedAirtablePhones: string[];
  snackBarMsg: string;
  selectedSlug: string;
}
