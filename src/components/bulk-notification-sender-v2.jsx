import {
  Button,
  Grid,
  Paper,
  Snackbar,
  TextField,
  Typography,
  Switch,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@material-ui/core";
import RefreshIcon from "@mui/icons-material/Refresh";
import React from "react";
import ChipInput from "material-ui-chip-input";
import * as Airtable from "airtable";
import { useEffect } from "react";
import useLocalStorage from "./useLocalStorage";
import DeleteIcon from "@mui/icons-material/Delete";

const BulkNotificationSenderV2 = () => {
  const base = new Airtable({ apiKey: "keyWMrmMF2FpUCVHW" }).base(
    "app7v46yarZKZVqYc"
  );


  const [phones, setPhones] = useLocalStorage("phonesv2", []);
  const [urls, setUrls] = useLocalStorage("urlsv2", [""]);
  const [label, setLabels] = useLocalStorage("typev2", [""]);
  const [type, setType] = useLocalStorage("labelsv2", [""]);

  const [open, setOpen] = useLocalStorage("openv2", false);
  const [getFromAirtable, setGetFromAirtable] = useLocalStorage(
    "getFromAirtableBoolen",
    false
  );
  const [airtableData, setAirtableData] = useLocalStorage("airtableDatav2", []);
  const [selectedAirtablePhones, setSelectedAirtablePhones] = useLocalStorage(
    "selectedAirtablePhones",
    []
  );
  const [snackBarMsg, setSnackBarMsg] = useLocalStorage("snackBarMsgv2", "");

  const [slugs, setSlugs] = useLocalStorage("slugs", []);
  const [selectedSlug, setSelectedSlug] = useLocalStorage("selectedSlugv2", "");

  const handleAddPhone = (chip) => {
    setPhones([...phones, chip]);
  };

  const handleDeletePhone = (chip, indexToRemove) => {
    setPhones([...phones.filter((item, index) => index !== indexToRemove)]);
  };

  const addNewUrl = () => {
    setUrls([...urls, ""]);
    setLabels([...label, ""]);
  };

  const deleteUrl = (index) => {
    setUrls([...urls.slice(0, index), ...urls.slice(index + 1)]);
    setLabels([...label.slice(0, index), ...label.slice(index + 1)]);
    setType([...type.slice(0, index), ...type.slice(index + 1)]);

  };

  const onSelectedSlug = (selectedSlug) => {
    setSelectedSlug(selectedSlug);
    const filteredPhones = [];
    if (selectedSlug === "Todos") {
      setSelectedAirtablePhones([...airtableData]);
      return;
    }
    slugs.forEach((slug, index) => {
      if (slug === selectedSlug) {
        // es - 1 porque cuando se guarda el arreglo se le pone en el indice 0 un "Todos"
        filteredPhones.push(airtableData[index - 1]);
      }
    });
    setSelectedAirtablePhones([...filteredPhones]);
  };

  const openSnackBar = (msg) => {
    setOpen(true);
    setSnackBarMsg(msg);
  };

  const sendNotifications = (phonesToSend, url, type) => {
    const uniquePhones = [...new Set(phonesToSend)];
    uniquePhones.forEach((phone) => {
      if (phone === "") return;
      try {
        fetch(url, {
          method: "POST",
          body: JSON.stringify({ type, users:  phonesToSend.map(phone => {
            return {
              phone : '+'+phone, params: {}
            }
          }) }),
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJZYWxvQXBpS2V5IiwieWlkIjoiMjA2IiwieXQiOiJjb25zdW1lciJ9.X0ayex-ZwjBWo3zgNW4QRhlpyvTJmS_XMECFvY79UeM"
          },
        })
          .then((res) => res.json())
          .catch((error) => console.error("Error:", error))
          .then((response) => {
            console.log("Success:", response);
            openSnackBar("Sending notifications");
          });
      } catch (err) {}
    });
  };

  const getPhoneNumbersFromAirTable = async () => {
    let phones = [];
    let allSlugs = [];
    const baseResult = await base("phones").select({
      view: "Grid view",
    });
    await baseResult.eachPage(
      function page(records, fetchNextPage) {
        // This function (`page`) will get called for each page of records.

        records.forEach(function (record) {
          phones.push(record.get("phone"));
          allSlugs.push(record.get("slug"));
        });

        // To fetch the next page of records, call `fetchNextPage`.
        // If there are more records, `page` will get called again.
        // If there are no more records, `done` will get called.
        fetchNextPage();
      },
      function done(err) {
        setSlugs(["Todos", ...allSlugs]);
        if (err) {
          console.error(err);
          return;
        }
        setAirtableData(phones);
        setSelectedAirtablePhones(phones);
        onSelectedSlug(selectedSlug);
        openSnackBar("Data Fetched from airtable");
      }
    );
  };

  useEffect(() => {
    if (getFromAirtable) {
      getPhoneNumbersFromAirTable();
    }
    // eslint-disable-next-line
  }, [getFromAirtable]);

  const openVisualizer = () => {
    const url = "https://airtable-node-visualizer.vercel.app/slug/" + selectedSlug;
    window.open(url);
  };


  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: "100vh" }}
    >
      <Button style={{ marginBottom: 10 }} onClick={() => openVisualizer()} variant="outlined">
        See In Visualizer {selectedSlug}
      </Button>

      <Paper style={{ padding: "20px", my: 5 }} elevation={3}>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          spacing={2}
        >
          <Grid item>
            <Grid container alignItems="center">
              <Typography>Input</Typography>
              <Switch
                onChange={(e) => setGetFromAirtable(e.target.checked)}
                checked={getFromAirtable}
                inputProps={{ "aria-label": "ant design" }}
              />
              <Typography>Airtable</Typography>
            </Grid>
          </Grid>
          {getFromAirtable && (
            <Button
              onClick={() => getPhoneNumbersFromAirTable()}
              variant="outlined"
              endIcon={<RefreshIcon />}
            >
              Refresh Airtable Data
            </Button>
          )}
          <Grid item>
            <Typography variant="h5">Bulk Notification Sender</Typography>
          </Grid>
          <Grid item>
            {getFromAirtable ? (
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Slug</InputLabel>
                <Select
                  style={{ width: "500px" }}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={selectedSlug}
                  label="Slug"
                  onChange={(e) => onSelectedSlug(e.target.value)}
                >
                  {[...new Set(slugs)].map((slug) => (
                    <MenuItem value={slug}>{slug}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            ) : (
              <ChipInput
                label="Phone numbers (52133333333)"
                style={{ width: "500px" }}
                value={phones}
                onAdd={(chip) => handleAddPhone(chip)}
                onDelete={(chip, index) => handleDeletePhone(chip, index)}
              />
            )}
          </Grid>
          {urls.map((url, index) => {
            return (
              <Box
                style={{
                  marginTop: "50px",
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "flex-end",
                }}
              >
                <DeleteIcon
                  sx={{ marginLeft: "90%" }}
                  onClick={() => deleteUrl(index)}
                />
                <Grid>
                  <TextField
                    style={{ width: "100%", marginBottom: "10px" }}
                    id="standard-basic"
                    label="Label"
                    variant="standard"
                    value={label[index]}
                    onChange={(e) =>
                      setLabels([
                        ...label.map((label, i) =>
                          i === index ? e.target.value : label
                        ),
                      ])
                    }
                  />
                </Grid>
                <Grid>
                  <TextField
                    style={{ width: "100%", marginBottom: "10px" }}
                    id="standard-basic"
                    label="Type"
                    variant="standard"
                    value={type[index]}
                    onChange={(e) =>
                      setType([
                        ...type.map((t, i) =>
                          i === index ? e.target.value : t
                        ),
                      ])
                    }
                  />
                </Grid>
                <Grid item>
                  <TextField
                    style={{ width: "500px" }}
                    onChange={(e) =>
                      setUrls([
                        ...urls.map((url, i) =>
                          i === index ? e.target.value : url
                        ),
                      ])
                    }
                    value={url}
                    fullWidth
                    id="outlined-basic"
                    label="URL (https://...)"
                    variant="outlined"
                  />
                </Grid>
                <Grid item style={{ marginTop: "10px" }}>
                  <Button
                    color="primary"
                    style={{ width: "500px" }}
                    onClick={() =>
                      sendNotifications(
                        getFromAirtable ? selectedAirtablePhones : phones,
                        urls[index],
                        type[index]
                      )
                    }
                    sx={{ width: "500px", overflow: "hidden" }}
                    variant="contained"
                  >
                    {label[index]} - Send to{" "}
                    {getFromAirtable
                      ? selectedAirtablePhones.length
                      : phones.length}{" "}
                    Users from {getFromAirtable ? "Airtable" : "Input"}{" "}
                  </Button>
                </Grid>
              </Box>
            );
          })}
          <Button
            style={{ marginTop: "50px" }}
            color="secondary"
            fullWidth
            variant="contained"
            onClick={() => addNewUrl()}
          >
            Add New Url
          </Button>

          
        </Grid>

        <Snackbar
          open={open}
          autoHideDuration={2000}
          onClose={() => setOpen(false)}
          message={snackBarMsg}
        />
      </Paper>
    </Grid>
  );
};
export default BulkNotificationSenderV2;
