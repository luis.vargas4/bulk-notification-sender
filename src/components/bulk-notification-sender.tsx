import React, { useState } from "react";
import {
  Button,
  Grid,
  Paper,
  Snackbar,
  TextField,
  Typography,
  Switch,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Divider,
  colors,
  Card,
  CardHeader,
  CardContent,
} from "@material-ui/core";
import RefreshIcon from "@mui/icons-material/Refresh";
import ChipInput from "material-ui-chip-input";
import * as Airtable from "airtable";
import { useEffect } from "react";
import useLocalStorage from "./useLocalStorage";
import DeleteIcon from "@mui/icons-material/Delete";
import { IEnviroment } from "../interfaces/environment";
import { Modal } from "@mui/material";

const BulkNotificationSender = () => {
  //@ts-ignore
  const base = new Airtable({
    apiKey:
      "patsmxvwhDcK8Bz8V.3736e9c87bf37dbf6c454fba8e4b1d2ff9c071d35bd10d868c6f7644b66f6e5d",
  }).base("app7v46yarZKZVqYc");

  const [envName, setEnvName] = useState("Default");
  const [openCreateNewEnviromentModal, setOpenCreateNewEnviromentModal] =
    useState(false);

  const initialEnvironment: IEnviroment = {
    phones: [],
    urls: [""],
    labels: [""],
    open: false,
    getFromAirtable: false,
    airtableData: [],
    selectedAirtablePhones: [],
    snackBarMsg: "",
    selectedSlug: "",
  };

  const [environments, setEnvironments] = useLocalStorage<{
    [key: string]: IEnviroment;
  }>("environments", {
    Default: { ...initialEnvironment },
  });

  const createNewEnvironment = (newName: string) => {
    setEnvironments({
      ...environments,
      [newName]: { ...initialEnvironment },
    });
    setOpenCreateNewEnviromentModal(false);
    setEnvName(newName);
  };

  const [open, setOpen] = useLocalStorage<boolean>("open", false);
  const [getFromAirtable, setGetFromAirtable] = useLocalStorage(
    "getFromAirtableBoolen",
    false
  );
  const [airtableData, setAirtableData] = useLocalStorage<string[]>(
    "airtableData",
    []
  );
  const [snackBarMsg, setSnackBarMsg] = useLocalStorage<string>(
    "snackBarMsg",
    ""
  );

  const [slugs, setSlugs] = useLocalStorage<string[]>("slugs", []);

  const deleteEnvironment = (envToDelete: string) => {
    if (envToDelete === "Default") return;
    const { [envToDelete]: deletedEnvironment, ...restEnvironments } =
      environments;
    setEnvironments(restEnvironments);
    setEnvName("Default"); // Set the default environment after deletion
  };

  const handleAddPhone = (chip: string) => {
    setEnvironments({
      ...environments,
      [envName]: {
        ...environments[envName],
        phones: [...environments[envName].phones, chip],
      },
    });
  };

  const handleDeletePhone = (_: string, indexToRemove: number) => {
    setEnvironments({
      ...environments,
      [envName]: {
        ...environments[envName],
        phones: [
          ...environments[envName].phones.filter(
            (_: string, index: number) => index !== indexToRemove
          ),
        ],
      },
    });
  };

  const addNewUrl = () => {
    setEnvironments({
      ...environments,
      [envName]: {
        ...environments[envName],
        labels: [...environments[envName].labels, ""],
        urls: [...environments[envName].urls, ""],
      },
    });
  };

  const deleteUrl = (index: number) => {
    setEnvironments({
      ...environments,
      [envName]: {
        ...environments[envName],
        urls: [
          ...environments[envName].urls.slice(0, index),
          ...environments[envName].urls.slice(index + 1),
        ],
        labels: [
          ...environments[envName].labels.slice(0, index),
          ...environments[envName].labels.slice(index + 1),
        ],
      },
    });
  };

  const onSelectedSlug = (selectedSlug: string) => {
    const filteredPhones: string[] = [];
    if (selectedSlug === "Todos") {
      setEnvironments({
        ...environments,
        [envName]: {
          ...environments[envName],
          selectedAirtablePhones: [...airtableData],
        },
      });
      return;
    }
    slugs.forEach((slug: string, index: number) => {
      if (slug === selectedSlug) {
        // es - 1 porque cuando se guarda el arreglo se le pone en el indice 0 un "Todos"
        filteredPhones.push(airtableData[index - 1]);
      }
    });
    setEnvironments({
      ...environments,
      [envName]: {
        ...environments[envName],
        selectedAirtablePhones: [...filteredPhones],
        selectedSlug,
      },
    });
  };

  const openSnackBar = (msg: string) => {
    setOpen(true);
    setSnackBarMsg(msg);
  };
  const sendNotifications = (phonesToSend: string[], url: string) => {
    const uniquePhones = [...new Set(phonesToSend)];
    uniquePhones.forEach((phone) => {
      if (phone === "") return;
      try {
        if (process.env?.REACT_APP_ENVIOROMENT === "production") {
          fetch(url, {
            method: "POST",
            body: JSON.stringify({ userId: phone }),
            headers: {
              "Content-Type": "application/json",
            },
          })
            .then((res) => res.json())
            .catch((error) => console.error("Error:", error))
            .then((response) => {
              console.log("Success:", response);
              openSnackBar("Sending notifications");
            });
        } else {
          console.log(
            "Mandando: ",
            url,
            phone,
            process.env.REACT_APP_ENVIOROMENT
          );
        }
      } catch (err) {}
    });
  };

  const getPhoneNumbersFromAirTable = async () => {
    let phones: string[] = [];
    let allSlugs: string[] = [];
    const baseResult = await base("phones").select({
      view: "Grid view",
    });
    await baseResult.eachPage(
      function page(records: any, fetchNextPage: any) {
        records.forEach(function (record: any) {
          phones.push(record.get("phone"));
          allSlugs.push(record.get("slug"));
        });
        fetchNextPage();
      },
      function done(err: any) {
        setSlugs(["Todos", ...allSlugs]);
        if (err) {
          console.error(err);
          return;
        }
        setAirtableData(phones);
        setEnvironments({
          ...environments,
          [envName]: {
            ...environments[envName],
            selectedAirtablePhones: phones,
          },
        });
        onSelectedSlug(environments[envName].selectedSlug);
        openSnackBar("Data Fetched from airtable");
      }
    );
  };

  useEffect(() => {
    if (getFromAirtable) {
      getPhoneNumbersFromAirTable();
    }
    // eslint-disable-next-line
  }, [getFromAirtable]);

  const openVisualizer = () => {
    const url =
      "https://airtable-node-visualizer.vercel.app/slug/" +
      environments[envName].selectedSlug;
    window.open(url);
  };

  return (
    <>
      <Modal
        open={openCreateNewEnviromentModal}
        onClose={() => setOpenCreateNewEnviromentModal(false)}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <CreateNewEnvironmentModal onCreate={createNewEnvironment} />
      </Modal>
      <Grid
        container
        spacing={2}
        direction="column"
        alignItems="center"
        justifyContent="center"
        style={{ minHeight: "100vh" }}
      >
        <Grid item>
          <Card style={{ width: 550 }}>
            <CardHeader
              title={
                <Typography variant="h5">Environment Selection:</Typography>
              }
            />

            <CardContent
              style={{ gap: 10, display: "flex", flexDirection: "column" }}
            >
              <FormControl fullWidth>
                <Select
                  style={{ width: "100%" }}
                  labelId="demo-simple-select-label"
                  variant="outlined"
                  id="demo-simple-select"
                  value={envName}
                  label="Slug"
                  onChange={(e) => setEnvName(e.target.value as string)}
                >
                  {Object.keys(environments).map((name) => (
                    <MenuItem key={name} value={name}>
                      {name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Button
                color="primary"
                style={{ width: "100%" }}
                variant="outlined"
                onClick={() => setOpenCreateNewEnviromentModal(true)}
              >
                Create New Environment
              </Button>

              <Button
                color="primary"
                style={{ width: "100%", backgroundColor: "red" }}
                variant="contained"
                onClick={() => deleteEnvironment(envName)}
              >
                Delete Environment
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <Grid item>
          <Paper
            style={{
              padding: "20px",
              marginBottom: 5,
              marginTop: 5,
              borderRadius: 10,
            }}
            elevation={5}
          >
            <Button
              style={{ marginBottom: 10, textDecoration: "underline" }}
              onClick={() => openVisualizer()}
              variant="text"
            >
              See In Visualizer {environments[envName].selectedSlug}
            </Button>
            <Grid style={{ marginBottom: 20 }} item>
              <Typography variant="h5">
                Bulk Notification Sender ENV: {envName}
              </Typography>
            </Grid>
            <Grid
              container
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              <Grid item>
                <Grid container alignItems="center">
                  <Typography>Input</Typography>
                  <Switch
                    onChange={(e) => setGetFromAirtable(e.target.checked)}
                    checked={getFromAirtable}
                    inputProps={{ "aria-label": "ant design" }}
                  />
                  <Typography>Airtable</Typography>
                </Grid>
              </Grid>
              {getFromAirtable && (
                <Button
                  onClick={() => getPhoneNumbersFromAirTable()}
                  variant="outlined"
                  endIcon={<RefreshIcon />}
                >
                  Refresh Airtable Data
                </Button>
              )}
              <Divider
                style={{ width: "100%", marginTop: 20, marginBottom: 20 }}
              />

              <Grid item>
                {getFromAirtable ? (
                  <Box style={{ width: "100%" }}>
                    <Typography
                      variant="h6"
                      align="left"
                      style={{ marginBottom: 10 }}
                    >
                      Slug Selection
                    </Typography>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label">
                        Slug
                      </InputLabel>
                      <Select
                        style={{ width: "500px" }}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={environments[envName].selectedSlug}
                        label="Slug"
                        onChange={(e) =>
                          onSelectedSlug(e.target.value as string)
                        }
                      >
                        {([...new Set(slugs)] as string[]).map((slug) => (
                          <MenuItem key={slug} value={slug}>
                            {slug}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Box>
                ) : (
                  <ChipInput
                    label="Phone numbers (52133333333)"
                    style={{ width: "500px" }}
                    value={environments[envName].phones}
                    onAdd={(chip) => handleAddPhone(chip)}
                    onDelete={(chip, index) => handleDeletePhone(chip, index)}
                  />
                )}
              </Grid>
              <Divider
                style={{ width: "100%", marginTop: 40, marginBottom: 20 }}
              />
              <Box>
                <Typography variant="h6" align="left">
                  URL Section
                </Typography>

                {environments[envName].urls.map(
                  (url: string, index: number) => {
                    return (
                      <Box
                        key={"urlLable" + index}
                        style={{
                          marginTop: "50px",
                          padding: 20,
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "flex-end",
                          backgroundColor: colors.grey[100],
                        }}
                      >
                        <DeleteIcon
                          sx={{ marginLeft: "90%" }}
                          onClick={() => deleteUrl(index)}
                        />
                        <Grid>
                          <TextField
                            style={{ width: "100%", marginBottom: "10px" }}
                            id="standard-basic"
                            label="Label"
                            variant="standard"
                            value={environments[envName].labels[index]}
                            onChange={(e) =>
                              setEnvironments({
                                ...environments,
                                [envName]: {
                                  ...environments[envName],
                                  labels: [
                                    ...environments[envName].labels.map(
                                      (label: string, i: number) =>
                                        i === index ? e.target.value : label
                                    ),
                                  ],
                                },
                              })
                            }
                          />
                        </Grid>
                        <Grid item>
                          <TextField
                            style={{ width: "500px" }}
                            onChange={(e) =>
                              setEnvironments({
                                ...environments,
                                [envName]: {
                                  ...environments[envName],
                                  urls: [
                                    ...environments[envName].urls.map(
                                      (url: string, i: number) =>
                                        i === index ? e.target.value : url
                                    ),
                                  ],
                                },
                              })
                            }
                            value={url}
                            fullWidth
                            id="outlined-basic-url"
                            label="URL (https://...)"
                            variant="outlined"
                          />
                        </Grid>
                        <Grid item style={{ marginTop: "10px" }}>
                          <Button
                            color="primary"
                            style={{ width: "500px", overflow: "hidden" }}
                            onClick={() =>
                              sendNotifications(
                                getFromAirtable
                                  ? environments[envName].selectedAirtablePhones
                                  : environments[envName].phones,
                                environments[envName].urls[index]
                              )
                            }
                            variant="contained"
                          >
                            {environments[envName].labels[index]} - Send to{" "}
                            {getFromAirtable
                              ? environments[envName].selectedAirtablePhones
                                  .length
                              : environments[envName].phones.length}{" "}
                            Users from {getFromAirtable ? "Airtable" : "Input"}{" "}
                          </Button>
                        </Grid>
                      </Box>
                    );
                  }
                )}
              </Box>
              <Divider
                style={{ width: "100%", marginTop: 20, marginBottom: 20 }}
              />

              <Button
                style={{ marginTop: "50px" }}
                color="secondary"
                fullWidth
                variant="contained"
                onClick={() => addNewUrl()}
              >
                Add New Url
              </Button>
            </Grid>

            <Snackbar
              open={open}
              autoHideDuration={2000}
              onClose={() => setOpen(false)}
              message={snackBarMsg}
            />
          </Paper>
        </Grid>
      </Grid>
    </>
  );
};
export default BulkNotificationSender;

const CreateNewEnvironmentModal = ({
  onCreate,
}: {
  onCreate: (e: string) => void;
}) => {
  const [name, setName] = useState("");
  return (
    <Card
      style={{
        position: "absolute" as "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        boxShadow: "24",

        width: 600,
        padding: 20,
      }}
    >
      <CardHeader title="Create New Environmen" />
      <TextField
        value={name}
        onChange={(e) => setName(e.target.value)}
        variant="outlined"
        fullWidth
        label="Name of new enviroment"
      />
      <Button
        style={{ marginTop: 10 }}
        variant="contained"
        fullWidth
        onClick={() => onCreate(name)}
      >
        Create {name} enviroment
      </Button>
    </Card>
  );
};
